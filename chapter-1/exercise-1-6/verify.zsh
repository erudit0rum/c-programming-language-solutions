#!/bin/zsh
echo "this script should print a line of ones and then one final zero, 3 times" | ./verify.out
echo "it's doing this because I am echoing three strings and then piping them each into a call to verify" | ./verify.out
echo "the reason it's printing ones and zeros is because there are no built in booleans in C" | ./verify.out