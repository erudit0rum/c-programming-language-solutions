#include <stdio.h>

int main()
{
    int is_not_EOF;
    is_not_EOF = getchar() != EOF;
    while (is_not_EOF) {
        printf("%d", is_not_EOF);
        is_not_EOF = getchar() != EOF;
    }
    printf("%d", is_not_EOF);
}