#include <stdio.h>

int main()
{
    float fahr, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    printf("%9s %9s\n", "celsius", "farenheit");
    celsius = lower;
    while (celsius <= upper) {
        fahr = (celsius / (5.0/9.0)) + 32;
        printf("%9.0f %9.1f\n", celsius, fahr);
        celsius = celsius + step;
    }
}